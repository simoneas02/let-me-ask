import copyImg from '../../assets/images/copy.svg'

import './style.scss'

type RoomCodeProps = {
  code: string
}

export const RoomCode = ({ code }: RoomCodeProps) => {
  const copyRoomCodeToClipboard = () => navigator.clipboard.writeText(code)

  return (
    <button className="room-code" onClick={copyRoomCodeToClipboard}>
      <div className="room-code-image-box">
        <img src={copyImg} alt="Copy room code" />
      </div>
      <p className="room-code-text">Sala: {code}</p>
    </button>
  )
}
