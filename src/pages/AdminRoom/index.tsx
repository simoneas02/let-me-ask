import { useHistory, useParams } from 'react-router-dom'

import { database } from '../../services/firebase'

import { useRoom } from '../../hooks/useRoom'

import { Button } from '../../components/Button'
import { RoomCode } from '../../components/RoomCode'
import { Question } from '../../components/Question'

import deleteImage from '../../assets/images/delete.svg'
import checkImage from '../../assets/images/check.svg'
import answerImage from '../../assets/images/answer.svg'

import './style.scss'

import logoImg from '../../assets/images/logo.svg'
import React from 'react'

type RoomParams = {
  id: string
}

export const AdminRoom = () => {
  const history = useHistory()
  const { id } = useParams<RoomParams>()
  const { questions, title } = useRoom(id)

  const handleCloseRoom = async () => {
    await database.ref(`rooms/${id}`).update({ closedAt: new Date() })

    history.push('/')
  }

  const handleDeleteQuestion = async (questionId: string) => {
    const deletConfirm = window.confirm(
      'Tem certeza que você deseja excluir esta pergunta?'
    )

    if (deletConfirm) {
      await database.ref(`rooms/${id}/questions/${questionId}`).remove()
    }
  }

  const handleCheckQuestionAsAnswered = async (questionId: string) => {
    await database
      .ref(`rooms/${id}/questions/${questionId}`)
      .update({ isAnswered: true })
  }
  const handleHighlightQuestion = async (questionId: string) => {
    await database
      .ref(`rooms/${id}/questions/${questionId}`)
      .update({ isHighlighted: true })
  }

  return (
    <div className="room-page">
      <header className="room-page-header">
        <div className="room-page-header-content">
          <img
            className="room-page-header-img"
            src={logoImg}
            alt="Let me ask"
          />

          <div className="admin-room-btn-group">
            <RoomCode code={id} />
            <Button isOutlined onClick={handleCloseRoom}>
              Encerrar sala
            </Button>
          </div>
        </div>
      </header>

      <main className="room-page-main">
        <header className="room-page-main-header">
          <h1 className="room-page-main-header-title">Sala {title}</h1>

          {questions.length > 0 && (
            <p className="room-page-main-header-question-count">
              {questions.length}
              {questions.length > 1 ? 'perguntas' : 'pergunta'}
            </p>
          )}
        </header>

        <ul className="question-list">
          {questions.map(
            ({ content, author, id, isAnswered, isHighlighted }) => (
              <Question
                key={id}
                content={content}
                author={author}
                isAnswered={isAnswered}
                isHighlighted={isHighlighted}
              >
                {!isAnswered && (
                  <>
                    <button
                      onClick={() => handleCheckQuestionAsAnswered(id)}
                      area-label="Marcar pergunta como respondida"
                      type="button"
                    >
                      <img
                        src={checkImage}
                        alt="Marcar pergunta como respondida"
                      />
                    </button>
                    <button
                      onClick={() => handleHighlightQuestion(id)}
                      area-label="Dar destaque à pergunta"
                      type="button"
                    >
                      <img src={answerImage} alt="Dar destaque à pergunta" />
                    </button>
                  </>
                )}

                <button
                  onClick={() => handleDeleteQuestion(id)}
                  area-label="Remover pergunta"
                  type="button"
                >
                  <img src={deleteImage} alt="Remover pergunta" />
                </button>
              </Question>
            )
          )}
        </ul>
      </main>
    </div>
  )
}
