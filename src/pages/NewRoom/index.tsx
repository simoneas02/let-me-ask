import { FormEvent, useState } from 'react'

import { database } from '../../services/firebase'
import { useAuth } from '../../hooks/useAuth'

import { Button } from '../../components/Button'
import { Link, useHistory } from 'react-router-dom'

import illustrationImg from '../../assets/images/illustration.svg'
import logoImg from '../../assets/images/logo.svg'

import './style.scss'

export const NewRoom = () => {
  const { user } = useAuth()
  const [newRoom, setNewRoom] = useState('')
  const history = useHistory()

  const handleCreateRoom = async (event: FormEvent) => {
    event.preventDefault()

    if (newRoom.trim() === '') return

    const roomRef = database.ref('rooms')

    const firebaseRoom = await roomRef.push({
      title: newRoom,
      authorId: user?.id,
    })

    history.push(`/rooms/${firebaseRoom.key}`)
  }

  return (
    <div className="home-page">
      <aside className="home-info">
        <img
          className="home-info-illustration"
          src={illustrationImg}
          alt="Illustration to questions and answers"
        />
        <h2 className="home-info-title">Crie salas de Q&amp;A ao vivo</h2>
        <p className="home-info-text">
          Tire as dúvidas da sua audiência em tempo real
        </p>
      </aside>

      <main className="home-main">
        <div className="home-main-content">
          <img className="home-main-logo" src={logoImg} alt="let me ask" />

          <h2 className="create-new-room-text">Criar uma nova sala</h2>

          <form onSubmit={handleCreateRoom}>
            <input
              className="home-main-form-input"
              type="text"
              name=""
              id=""
              placeholder="Nome da sala"
              onChange={event => setNewRoom(event.target.value)}
              value={newRoom}
            />
            <Button type="submit">Criar sala</Button>
          </form>

          <p className="go-to-new-room">
            Quer entrar em uma sala existente?{' '}
            <Link className="go-to-new-room-link" to="/">
              clique aqui
            </Link>
          </p>
        </div>
      </main>
    </div>
  )
}
