import { FormEvent, useState } from 'react'
import { useHistory } from 'react-router-dom'

import { Button } from '../../components/Button'

import { database } from '../../services/firebase'
import { useAuth } from '../../hooks/useAuth'

import illustrationImg from '../../assets/images/illustration.svg'
import logoImg from '../../assets/images/logo.svg'
import googleIcon from '../../assets/images/google-icon.svg'

import './style.scss'

export const Home = () => {
  const history = useHistory()
  const [roomCode, setRoomCode] = useState('')

  const { user, signInWithGoogle } = useAuth()

  const handleCreateRoom = async () => {
    if (!user) {
      await signInWithGoogle()
    }

    history.push('/rooms/new')
  }

  const handleJoinRoom = async (event: FormEvent) => {
    event.preventDefault()

    if (roomCode.trim() === '') return

    const roomRef = await database.ref(`rooms/${roomCode}`).get()

    if (!roomRef.exists()) {
      alert('Room does not exist.')
      return
    }

    if (roomRef.val().closedAt) {
      alert('Room already closed.')
      return
    }

    history.push(`rooms/${roomCode}`)
  }

  return (
    <div className="home-page">
      <aside className="home-info">
        <img
          className="home-info-illustration"
          src={illustrationImg}
          alt="Illustration to questions and answers"
        />
        <h2 className="home-info-title">Crie salas de Q&amp;A ao vivo</h2>
        <p className="home-info-text">
          Tire as dúvidas da sua audiência em tempo real
        </p>
      </aside>

      <main className="home-main">
        <div className="home-main-content">
          <img className="home-main-logo" src={logoImg} alt="let me ask" />

          <button className="home-main-create-room" onClick={handleCreateRoom}>
            <img
              className="home-main-create-room-img"
              src={googleIcon}
              alt="google logo"
            />
            Crie sua sala com o Google
          </button>

          <p className="home-main-text">ou entre em uma sala</p>

          <form onSubmit={handleJoinRoom}>
            <input
              className="home-main-form-input"
              type="text"
              name=""
              id=""
              placeholder="Digite o código da sala"
              onChange={event => setRoomCode(event.target.value)}
              value={roomCode}
            />
            <Button type="submit">Entrar na sala</Button>
          </form>
        </div>
      </main>
    </div>
  )
}
