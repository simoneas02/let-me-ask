import classNames from 'classnames'
import { ReactNode } from 'react'

import './style.scss'

type QuestionProps = {
  author: {
    name: string
    avatar: string
  }
  content: string
  children?: ReactNode
  isHighlighted?: boolean
  isAnswered?: boolean
}

export const Question = ({
  author: { avatar, name },
  content,
  children,
  isAnswered = false,
  isHighlighted = false,
}: QuestionProps) => (
  <li
    className={classNames(
      'question-container',
      { answered: isAnswered },
      { hihlighted: isHighlighted && !isAnswered }
    )}
  >
    <p className="question-content">{content}</p>
    <footer className="question-footer">
      <figure className="question-footer-figure">
        <img className="question-footer-img" src={avatar} alt={name} />
        <figcaption className="question-footer-figcaption">{name}</figcaption>
      </figure>

      <div className="question-children">{children}</div>
    </footer>
  </li>
)
